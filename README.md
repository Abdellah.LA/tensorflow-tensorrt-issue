# Tensorflow TensorRT Issue

Tensorflow TensorRT (TF-TRT) Slow inference time issue for Object Detection Models, tested on EfficientDet D0-D7 and SSD Resnet50 640x640

- [Notebook 0](https://gitlab.com/Abdellah.LA/tensorflow-tensorrt-issue/-/blob/main/0-%20Tutorial%20:%20Tensorflow%20TensorRT%20Integration.ipynb)
contains the results provided by the TensorRT examples guide, and does reduce inference time significantly on the Resnet50 Keras based model
- [Notebook 1](https://gitlab.com/Abdellah.LA/tensorflow-tensorrt-issue/-/blob/main/1-%20Using%20SSD%20Resnet50%20640x640%20:%20TF-TRT%20API.ipynb) contains the results by applying the same steps on the SSD Resnet640x640 model, and inference time practically remains unchanged
- [Notebook 2](https://gitlab.com/Abdellah.LA/tensorflow-tensorrt-issue/-/blob/main/2-%20Using%20EfficientDet%20D0%20512x512%20:%20TF-TRT%20API.ipynb) contains the results by also applying the same steps but on EfficentDet D0 512x512, inference time also didn't change
